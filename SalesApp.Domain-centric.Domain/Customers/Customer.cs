﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesApp.Domain_centric.Domain.Customers
{
    public class Customer
    {
        private Guid _customerId;

        public string CustomerId
        {
            get { return _customerId.ToString(); }
            set
            {
                _customerId = Guid.Parse(value);
            }
        }
        public string Name { get; set; }
    }
}
