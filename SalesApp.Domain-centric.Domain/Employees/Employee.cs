﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesApp.Domain_centric.Domain.Employees
{
    public class Employee
    {
        private Guid _employeeId;
        public string EmployeeId
        {
            get { return _employeeId.ToString(); }
            set
            {
                _employeeId = Guid.Parse(value);
            }
        }
        public string Name { get; set; }
    }
}
