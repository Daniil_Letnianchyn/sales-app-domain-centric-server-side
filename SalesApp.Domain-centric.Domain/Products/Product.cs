﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesApp.Domain_centric.Domain.Products
{
    public class Product
    {
        private Guid _productId;
        public string ProductId
        {
            get { return _productId.ToString(); }
            set
            {
                _productId = Guid.Parse(value);
            }
        }
        public decimal Price { get; set; }
        public string Name { get; set; }
    }
}
