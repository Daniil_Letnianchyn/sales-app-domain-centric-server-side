﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesApp.Domain_centric.Common.Logger
{
    public interface ICustomLogger
    {
        void Info(params string[] list);
        void Warn(params string[] list);
        void Debug(params string[] list);
        void Error(params string[] list);
    }
}
