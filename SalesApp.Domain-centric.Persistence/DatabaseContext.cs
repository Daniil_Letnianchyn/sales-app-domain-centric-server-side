﻿using Microsoft.EntityFrameworkCore;
using SalesApp.Domain_centric.Persistence.Entities;
using SalesApp.Domain_centric.Persistence.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SalesApp.Domain_centric.Persistence
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<Sale> Sales { get; set; }


        //public DbSet<Incident> Incidents { get; set; }
        //public DbSet<Comment> Comments { get; set; }
        //public DbSet<Profile> Profiles { get; set; }
        //public DbSet<Priority> Priorities { get; set; }
        //public DbSet<Status> Statuses { get; set; }
        //public DbSet<Role> Roles { get; set; }


        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
        }
        //public DatabaseContext() { }

        public override int SaveChanges()
        {
            SetModificationHistory();
            return base.SaveChanges();
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            SetModificationHistory();
            return await base.SaveChangesAsync(cancellationToken);
        }

        private void SetModificationHistory()
        {
            foreach (var history in ChangeTracker.Entries()
                                                .Where(e => e.Entity is IModificationHistory &&
                                                (e.State == EntityState.Added || e.State == EntityState.Modified))
                                                .Select(e => e.Entity as IModificationHistory))
            {
                history.DateModified = DateTimeOffset.Now;
                if (history.DateCreated == DateTimeOffset.MinValue)
                {
                    history.DateCreated = DateTimeOffset.Now;
                }
            }
        }



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // seed the database with dummy data
            modelBuilder.Entity<Customer>().HasData(
                new Customer()
                {
                    CustomerId = Guid.Parse("d28888e9-2ba9-473a-a40f-e38cb54f9b35"),
                    Name = "'The Dude' Lebowski",
                    DateCreated = DateTimeOffset.Now,
                    DateModified = DateTimeOffset.Now
                },
                new Customer()
                {
                    CustomerId = Guid.Parse("da2fd609-d754-4feb-8acd-c4f9ff13ba96"),
                    Name = "'The Big' Lebowski",
                    DateCreated = DateTimeOffset.Now,
                    DateModified = DateTimeOffset.Now
                },
                new Customer()
                {
                    CustomerId = Guid.Parse("2902b665-1190-4c70-9915-b9c2d7680450"),
                    Name = "Walter Sobchak",
                    DateCreated = DateTimeOffset.Now,
                    DateModified = DateTimeOffset.Now
                },
                new Customer()
                {
                    CustomerId = Guid.Parse("102b566b-ba1f-404c-b2df-e2cde39ade09"),
                    Name = "Theodore Donald 'Donny' Kerabatsos",
                    DateCreated = DateTimeOffset.Now,
                    DateModified = DateTimeOffset.Now
                }
            );

            modelBuilder.Entity<Employee>().HasData(
                new Employee
                {
                    EmployeeId = Guid.Parse("5b1c2b4d-48c7-402a-80c3-cc796ad49c6b"),
                    Name = "Caitlin Cleric",
                    DateCreated = DateTimeOffset.Now,
                    DateModified = DateTimeOffset.Now
                },
                new Employee
                {
                    EmployeeId = Guid.Parse("d8663e5e-7494-4f81-8739-6e0de1bea7ee"),
                    Name = "Solmyr Wizard",
                    DateCreated = DateTimeOffset.Now,
                    DateModified = DateTimeOffset.Now
                },
                new Employee
                {
                    EmployeeId = Guid.Parse("d173e20d-159e-4127-9ce9-b0ac2564ad97"),
                    Name = "Sandro Necromancer",
                    DateCreated = DateTimeOffset.Now,
                    DateModified = DateTimeOffset.Now
                },
                new Employee
                {
                    EmployeeId = Guid.Parse("40ff5488-fdab-45b5-bc3a-14302d59869a"),
                    Name = "Pasis Planeswalker",
                    DateCreated = DateTimeOffset.Now,
                    DateModified = DateTimeOffset.Now
                }
            );

            modelBuilder.Entity<Product>().HasData(
                new Product
                {
                    ProductId = Guid.Parse("a64549d1-8879-4c1f-ac89-e123fe633ad6"),
                    Name = "Spaghetti",
                    Price = 0.7m,
                    DateCreated = DateTimeOffset.Now,
                    DateModified = DateTimeOffset.Now
                },
                new Product
                {
                    ProductId = Guid.Parse("be939226-9038-4f43-950f-2e860b34d377"),
                    Name = "Lasagna",
                    Price = 1.63m,
                    DateCreated = DateTimeOffset.Now,
                    DateModified = DateTimeOffset.Now
                },
                new Product
                {
                    ProductId = Guid.Parse("42138ae2-3391-4a72-a7a0-607ac6da7b30"),
                    Name = "Ravioli",
                    Price = 6.01m,
                    DateCreated = DateTimeOffset.Now,
                    DateModified = DateTimeOffset.Now
                }
            );

            modelBuilder.Entity<Sale>().HasData(
                new Sale
                {
                    SaleId = Guid.Parse("48e9a975-dc47-40ae-85e2-ed2dec327b97"),
                    Quantity = 2,
                    TotalPrice = 3.26m,
                    CustomerId = Guid.Parse("d28888e9-2ba9-473a-a40f-e38cb54f9b35"),
                    EmployeeId = Guid.Parse("5b1c2b4d-48c7-402a-80c3-cc796ad49c6b"),
                    ProductId = Guid.Parse("be939226-9038-4f43-950f-2e860b34d377"),
                    DateCreated = DateTimeOffset.Now,
                    DateModified = DateTimeOffset.Now
                },
                new Sale
                {
                    SaleId = Guid.Parse("d4fdba84-1ff8-4fa2-9930-68e447623c8d"),
                    Quantity = 12,
                    TotalPrice = 72.12m,
                    CustomerId = Guid.Parse("2902b665-1190-4c70-9915-b9c2d7680450"),
                    EmployeeId = Guid.Parse("d173e20d-159e-4127-9ce9-b0ac2564ad97"),
                    ProductId = Guid.Parse("42138ae2-3391-4a72-a7a0-607ac6da7b30"),
                    DateCreated = DateTimeOffset.Now,
                    DateModified = DateTimeOffset.Now
                }
            );

            base.OnModelCreating(modelBuilder);
        }
    }
}
