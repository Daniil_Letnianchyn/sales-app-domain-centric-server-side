﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalesApp.Domain_centric.Persistence
{
    public interface IDatabaseService
    {
        Task<IEnumerable<SalesApp.Domain_centric.Domain.Sales.Sale>> GetAllSalesAsync();
        Task<SalesApp.Domain_centric.Domain.Sales.Sale> GetSaleByIdAsync(string saleId);
        Task<SalesApp.Domain_centric.Domain.Sales.Sale> CreateSaleAsync(SalesApp.Domain_centric.Domain.Sales.Sale saleForCreation);
        Task<IEnumerable<SalesApp.Domain_centric.Domain.Products.Product>> GetAllProductsAsync();
        Task<IEnumerable<SalesApp.Domain_centric.Domain.Employees.Employee>> GetAllEmployeesAsync();
        Task<IEnumerable<SalesApp.Domain_centric.Domain.Customers.Customer>> GetAllCustomersAsync();
    }
}
