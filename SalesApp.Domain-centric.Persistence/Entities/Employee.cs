﻿using SalesApp.Domain_centric.Persistence.Entities.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace SalesApp.Domain_centric.Persistence.Entities
{
    public class Employee : IModificationHistory
    {
        [Key]
        public Guid EmployeeId { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        public DateTimeOffset DateModified { get; set; }
        public DateTimeOffset DateCreated { get; set; }
    }
}
