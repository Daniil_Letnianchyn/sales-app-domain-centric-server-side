﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesApp.Domain_centric.Persistence.Entities.Interfaces
{
    public interface IModificationHistory
    {
        DateTimeOffset DateModified { get; set; }
        DateTimeOffset DateCreated { get; set; }
    }
}
