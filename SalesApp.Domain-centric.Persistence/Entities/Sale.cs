﻿using SalesApp.Domain_centric.Persistence.Entities.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SalesApp.Domain_centric.Persistence.Entities
{
    public class Sale : IModificationHistory
    {
        [Key]
        public Guid SaleId { get; set; }

        [Required]
        [ForeignKey("CustomerId")]
        public Customer Customer { get; set; }

        [Required]
        [ForeignKey("EmployeeId")]
        public Employee Employee { get; set; }

        [Required]
        [ForeignKey("ProductId")]
        public Product Product { get; set; }

        [Required]
        public int Quantity { get; set; }

        [Required]
        public decimal TotalPrice { get; set; }

        public Guid CustomerId { get; set; }
        public Guid EmployeeId { get; set; }
        public Guid ProductId { get; set; }

        public DateTimeOffset DateModified { get; set; }
        public DateTimeOffset DateCreated { get; set; }
    }
}
