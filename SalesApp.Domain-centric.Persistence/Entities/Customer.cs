﻿using SalesApp.Domain_centric.Persistence.Entities.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace SalesApp.Domain_centric.Persistence.Entities
{
    public class Customer : IModificationHistory
    {
        [Key]
        public Guid CustomerId { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        public DateTimeOffset DateModified { get; set; }
        public DateTimeOffset DateCreated { get; set; }
    }
}
