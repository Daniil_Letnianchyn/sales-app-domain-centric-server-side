﻿
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesApp.Domain_centric.Persistence
{
    public class DatabaseService : IDatabaseService
    {
        private DatabaseContext _dbContext;
        public DatabaseService(DatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<SalesApp.Domain_centric.Domain.Sales.Sale> CreateSaleAsync(SalesApp.Domain_centric.Domain.Sales.Sale saleForCreation)
        {
            Entities.Sale saleEntity = new Entities.Sale
            {
                Customer = new Entities.Customer { CustomerId = Guid.Parse(saleForCreation.Customer.CustomerId) },
                Employee = new Entities.Employee { EmployeeId = Guid.Parse(saleForCreation.Employee.EmployeeId) },
                Product = new Entities.Product { ProductId = Guid.Parse(saleForCreation.Product.ProductId) },
                Quantity = saleForCreation.Quantity,
                TotalPrice = saleForCreation.Quantity * _dbContext.Products.Where(i => i.ProductId == Guid.Parse(saleForCreation.Product.ProductId)).Select(v => v.Price).SingleOrDefault()
            };
            _dbContext.Attach(saleEntity);
            await _dbContext.SaveChangesAsync();

            return await _dbContext.Sales
                .Where(i => i.SaleId == saleEntity.SaleId)
                .Select(p => new SalesApp.Domain_centric.Domain.Sales.Sale
                {
                    SaleId = p.SaleId.ToString(),
                    Date = p.DateCreated,
                    Customer = new SalesApp.Domain_centric.Domain.Customers.Customer { CustomerId = p.Customer.CustomerId.ToString(), Name = p.Customer.Name },
                    Employee = new SalesApp.Domain_centric.Domain.Employees.Employee { EmployeeId = p.Employee.EmployeeId.ToString(), Name = p.Employee.Name },
                    Product = new SalesApp.Domain_centric.Domain.Products.Product { ProductId = p.Product.ProductId.ToString(), Name = p.Product.Name, Price = p.Product.Price },
                    Quantity = p.Quantity,
                    TotalPrice = p.TotalPrice,
                    UnitPrice = p.TotalPrice / p.Quantity,
                })
                .SingleOrDefaultAsync();
        }

        public async Task<IEnumerable<SalesApp.Domain_centric.Domain.Customers.Customer>> GetAllCustomersAsync()
        {
            return await _dbContext.Customers
                .Select(p => new SalesApp.Domain_centric.Domain.Customers.Customer
                {
                    CustomerId = p.CustomerId.ToString(),
                    Name = p.Name
                })
                .ToListAsync();
        }

        public async Task<IEnumerable<SalesApp.Domain_centric.Domain.Employees.Employee>> GetAllEmployeesAsync()
        {
            return await _dbContext.Employees
                .Select(p => new SalesApp.Domain_centric.Domain.Employees.Employee
                {
                    EmployeeId = p.EmployeeId.ToString(),
                    Name = p.Name
                })
                .ToListAsync();
        }

        public async Task<IEnumerable<SalesApp.Domain_centric.Domain.Products.Product>> GetAllProductsAsync()
        {
            return await _dbContext.Products
                .Select(p => new SalesApp.Domain_centric.Domain.Products.Product
                {
                    ProductId = p.ProductId.ToString(),
                    Name = p.Name,
                    Price = p.Price
                })
                .ToListAsync();
        }

        public async Task<IEnumerable<SalesApp.Domain_centric.Domain.Sales.Sale>> GetAllSalesAsync()
        {
            return await _dbContext.Sales
                .Select(p => new SalesApp.Domain_centric.Domain.Sales.Sale
                {
                    SaleId = p.SaleId.ToString(),
                    Date = p.DateCreated,
                    Customer = new SalesApp.Domain_centric.Domain.Customers.Customer { CustomerId = p.Customer.CustomerId.ToString(), Name = p.Customer.Name },
                    Employee = new SalesApp.Domain_centric.Domain.Employees.Employee { EmployeeId = p.Employee.EmployeeId.ToString(), Name = p.Employee.Name },
                    Product = new SalesApp.Domain_centric.Domain.Products.Product { ProductId = p.Product.ProductId.ToString(), Name = p.Product.Name, Price = p.Product.Price },
                    Quantity = p.Quantity,
                    TotalPrice = p.TotalPrice,
                    UnitPrice = p.TotalPrice / p.Quantity,
                })
                .ToListAsync();
        }

        public async Task<SalesApp.Domain_centric.Domain.Sales.Sale> GetSaleByIdAsync(string saleId)
        {
            return await _dbContext.Sales
                .Where(i => i.SaleId == Guid.Parse(saleId))
                .Select(p => new SalesApp.Domain_centric.Domain.Sales.Sale
                {
                    SaleId = p.SaleId.ToString(),
                    Date = p.DateCreated,
                    Customer = new SalesApp.Domain_centric.Domain.Customers.Customer { CustomerId = p.Customer.CustomerId.ToString(), Name = p.Customer.Name },
                    Employee = new SalesApp.Domain_centric.Domain.Employees.Employee { EmployeeId = p.Employee.EmployeeId.ToString(), Name = p.Employee.Name },
                    Product = new SalesApp.Domain_centric.Domain.Products.Product { ProductId = p.Product.ProductId.ToString(), Name = p.Product.Name, Price = p.Product.Price },
                    Quantity = p.Quantity,
                    TotalPrice = p.TotalPrice,
                    UnitPrice = p.TotalPrice / p.Quantity,
                })
                .SingleOrDefaultAsync();
        }
    }
}
