﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesApp.Domain_centric.Infrastructure.Inventory
{
    public interface IInventoryService
    {
        public void NotifySaleOcurred();
    }
}
