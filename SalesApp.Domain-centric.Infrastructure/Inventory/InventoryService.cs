﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesApp.Domain_centric.Infrastructure.Inventory
{
    public class InventoryService : IInventoryService
    {
        public void NotifySaleOcurred()
        {
            NotifyNoOne();
        }

        private void NotifyNoOne(){
        }
    }
}
