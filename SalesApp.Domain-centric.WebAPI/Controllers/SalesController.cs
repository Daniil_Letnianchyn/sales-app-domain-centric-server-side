﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SalesApp.Domain_centric.Common.Logger;
using SalesApp.Domain_centric.Infrastructure.Inventory;
using SalesApp.Domain_centric.Persistence;

namespace SalesApp.Domain_centric.WebAPI.Controllers
{
    [Route("api/sales")]
    [ApiController]
    public class SalesController : ControllerBase
    {
        private IDatabaseService _databaseService;
        private ICustomLogger _logger;
        private IInventoryService _inventory;

        public SalesController(IDatabaseService databaseService, ICustomLogger logger, IInventoryService inventory)
        {
            this._databaseService = databaseService;
            this._logger = logger;
            this._inventory = inventory;
        }

        [HttpGet()]
        public async Task<IActionResult> GetSalesAsync()
        {
            IEnumerable<SalesApp.Domain_centric.Domain.Sales.Sale> allSales = await _databaseService.GetAllSalesAsync();

            IEnumerable<DataTransferObjects.Outgoing.Sale> allSalesDTO = allSales
                .Select(b => new DataTransferObjects.Outgoing.Sale
                {
                    Id = b.SaleId,
                    Customer = b.Customer.Name,
                    Employee = b.Employee.Name,
                    Product = b.Product.Name,
                    Date = b.Date,
                    Quantity = b.Quantity,
                    TotalPrice = b.TotalPrice,
                    UnitPrice = b.UnitPrice
                });
            return Ok(allSalesDTO);
        }


        [HttpGet("{saleId}")]
        public async Task<IActionResult> GetSaleByIdAsync(string saleId)
        {
            SalesApp.Domain_centric.Domain.Sales.Sale sale = await _databaseService.GetSaleByIdAsync(saleId);

            if (sale == null)
            {
                return NotFound();
            }

            DataTransferObjects.Outgoing.Sale saleToReturnDTO = new DataTransferObjects.Outgoing.Sale
            {
                Id = sale.SaleId,
                Customer = sale.Customer.Name,
                Employee = sale.Employee.Name,
                Product = sale.Product.Name,
                Date = sale.Date,
                Quantity = sale.Quantity,
                TotalPrice = sale.TotalPrice,
                UnitPrice = sale.UnitPrice
            };

            return Ok(saleToReturnDTO);
        }

        [HttpPost()]
        public async Task<IActionResult> AddSaleAsync([FromBody] DataTransferObjects.Incoming.SaleForCreation saleForCreationDTO)
        {
            if (saleForCreationDTO == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            SalesApp.Domain_centric.Domain.Sales.Sale saleForCreation = new SalesApp.Domain_centric.Domain.Sales.Sale
            {
                Customer = new SalesApp.Domain_centric.Domain.Customers.Customer { CustomerId = saleForCreationDTO.CustomerId },
                Employee = new SalesApp.Domain_centric.Domain.Employees.Employee { EmployeeId = saleForCreationDTO.EmployeeId },
                Product = new SalesApp.Domain_centric.Domain.Products.Product { ProductId = saleForCreationDTO.ProductId },
                Quantity = saleForCreationDTO.Quantity
            };

            SalesApp.Domain_centric.Domain.Sales.Sale sale = await _databaseService.CreateSaleAsync(saleForCreation);
            _logger.Info($@"Sale {saleForCreation.SaleId} was crated");
            _inventory.NotifySaleOcurred();

            DataTransferObjects.Outgoing.Sale saleToReturnDTO = new DataTransferObjects.Outgoing.Sale
            {
                Id = sale.SaleId,
                Customer = sale.Customer.Name,
                Employee = sale.Employee.Name,
                Product = sale.Product.Name,
                Date = sale.Date,
                Quantity = sale.Quantity,
                TotalPrice = sale.TotalPrice,
                UnitPrice = sale.UnitPrice
            };
            return Ok(saleToReturnDTO);
        }
    }
}