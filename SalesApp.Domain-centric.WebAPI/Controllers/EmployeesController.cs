﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SalesApp.Domain_centric.Persistence;

namespace SalesApp.Domain_centric.WebAPI.Controllers
{
    [Route("api/employees")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private IDatabaseService _databaseService;

        public EmployeesController(IDatabaseService databaseService)
        {
            this._databaseService = databaseService;
        }

        [HttpGet()]
        public async Task<IActionResult> GetEmployeesAsync()
        {
            IEnumerable<SalesApp.Domain_centric.Domain.Employees.Employee> allEmployees = await _databaseService.GetAllEmployeesAsync();

            IEnumerable<DataTransferObjects.Outgoing.Employee> allEmployeesDTO = allEmployees
                .Select(b => new DataTransferObjects.Outgoing.Employee
                {
                    Id = b.EmployeeId,
                    Name = b.Name
                });
            return Ok(allEmployeesDTO);
        }
    }
}