﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SalesApp.Domain_centric.Persistence;

namespace SalesApp.Domain_centric.WebAPI.Controllers
{
    [Route("api/customers")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private IDatabaseService _databaseService;

        public CustomersController(IDatabaseService databaseService)
        {
            this._databaseService = databaseService;
        }

        [HttpGet()]
        public async Task<IActionResult> GetCustomersAsync()
        {
            IEnumerable<SalesApp.Domain_centric.Domain.Customers.Customer> allCustomers = await _databaseService.GetAllCustomersAsync();

            IEnumerable<DataTransferObjects.Outgoing.Customer> allCustomersDTO = allCustomers
                .Select(b => new DataTransferObjects.Outgoing.Customer 
                { 
                    Id = b.CustomerId,
                    Name = b.Name                
                });
            return Ok(allCustomersDTO);
        }
    }
}