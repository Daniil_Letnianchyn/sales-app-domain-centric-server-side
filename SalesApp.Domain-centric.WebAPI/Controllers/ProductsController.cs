﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SalesApp.Domain_centric.Persistence;

namespace SalesApp.Domain_centric.WebAPI.Controllers
{
    [Route("api/products")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private IDatabaseService _databaseService;

        public ProductsController(IDatabaseService databaseService)
        {
            this._databaseService = databaseService;
        }

        [HttpGet()]
        public async Task<IActionResult> GetProductsAsync()
        {
            IEnumerable<SalesApp.Domain_centric.Domain.Products.Product> allProducts = await _databaseService.GetAllProductsAsync();

            IEnumerable<DataTransferObjects.Outgoing.Product> allPproductsDTO = allProducts
                .Select(b => new DataTransferObjects.Outgoing.Product
                {
                    Id = b.ProductId,
                    Name = b.Name,
                    Price = b.Price                    
                });


            return Ok(allPproductsDTO);
        }
    }
}