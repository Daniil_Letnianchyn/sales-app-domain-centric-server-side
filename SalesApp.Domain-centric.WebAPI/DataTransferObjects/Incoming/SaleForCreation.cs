﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalesApp.Domain_centric.WebAPI.DataTransferObjects.Incoming
{
    public class SaleForCreation
    {
        public string CustomerId { get; set; }
        public string EmployeeId { get; set; }
        public string ProductId { get; set; }
        public int Quantity { get; set; }
    }
}
